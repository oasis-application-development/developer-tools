#!/bin/bash

curl https://gitlab.oit.duke.edu/oasis-application-development/developer-tools/-/raw/v1.5.1/local_kubernetes/$(uname -s)/install.sh | bash || exit 1
echo "Ready for Local Kubernetes Development"
