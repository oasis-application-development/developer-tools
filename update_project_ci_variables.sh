#!/bin/bash
: ${TARGET_PROJECT_ID?"TARGET_PROJECT_ID environment required."}

CI_API_V4_URL="${CI_API_V4_URL:-https://gitlab.oit.duke.edu/api/v4}"
header_file=${HEADER_FILE:-${HOME}/.dhe_gitlab_token_personal_headers}
if [ ! -f "${header_file}" ]
then
  echo "credentials file required!"
  exit 1
fi

while IFS= read -r line
do
  scope=$(echo ${line} | cut -d: -f1)
  key=$(echo ${line} | cut -d: -f2)
  value=$(echo ${line} | cut -d: -f3)
  curl -s -H @${header_file} \
          -X PUT \
          "${CI_API_V4_URL}/projects/${TARGET_PROJECT_ID}/variables/${key}?filter\[environment_scope\]=${scope}" \
          --form "value=${value}"
done

