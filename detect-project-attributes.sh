#!/bin/bash

supported_database() {
  local gem=$1
cat <<EOF | grep ${gem} > /dev/null 2>&1
  pg
  activerecord-oracle_enhanced-adapter
  sqlite3
EOF
}

database_for()
{
  local gem=$1
  case "${gem}" in
    pg)
      echo 'postgres'
      ;;
    activerecord-oracle_enhanced-adapter)
      echo 'oracle'
      ;;
    sqlite3)
      echo 'sqlite'
      ;;
  esac
}

supported_service() {
  local gem=$1
cat <<EOF | grep ${gem} > /dev/null 2>&1
  redis
EOF
}

service_for()
{
  local gem=$1
  case "${gem}" in
    redis)
      echo 'redis'
      ;;
  esac
}

detect_ruby_gemfile_version()
{
  input="$(grep '^ruby' Gemfile)"
  if [ $? -gt 0 ]
  then
    echo "You must include ruby '~> X.Y.Z' in your Gemfile for this to detect your ruby version" >&2
    return 1
  fi

  regex="^ruby.*~>.*([1-9]+.[0-9]+.[0-9]+).*$"
  if [[ $input =~ $regex ]]
  then
    ruby_version=${BASH_REMATCH[1]}
    echo "${ruby_version}"
  else
    echo "You must include ruby '~> X.Y.Z' in your Gemfile for this to detect your ruby version" >&2
    return 1
  fi
}

detect_ruby_dotfile_version()
{
  input=$(cat .ruby-version)
  regex="^ruby-([1-9]+.[0-9]+.[0-9]+)$"
  if [[ $input =~ $regex ]]
  then
    ruby_version=${BASH_REMATCH[1]}
    echo "${ruby_version}"
  else
    detect_ruby_gemfile_version
  fi
}

# try .ruby-version first, since its easier to parse
# fall-back to Gemfile
detect_ruby_version() {
  if [ -f .ruby-version ]
  then
    detect_ruby_dotfile_version
  else
    detect_ruby_gemfile_version
  fi
}

detect_project_type()
{
  if [ -f Gemfile ]
  then
    echo 'ruby'
  else
      echo 'only ruby projects with a Gemfile are supported' >&2
      exit 1
  fi
}

detect_project_type_version()
{
  local type=$1
  case "${type}" in
    ruby)
      detect_ruby_version
      ;;
    *)
      echo "unsupported type ${type}" >&2
      exit 1
      ;;
  esac
}

if [ -e ".devtools" ]
then
  echo "project type already detected, remove .devtools to detect new settings"
  exit
fi

jq --version > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "please install jq https://stedolan.github.io/jq/download" >&2
  exit 1
fi

project_name=${PWD##*/}
project_type=$(detect_project_type) || exit 1
project_type_version=$(detect_project_type_version "${project_type}") || exit 1
project_upstream_base_image=${PROJECT_UPSTREAM_BASE_IMAGE:-"${project_type}:${project_type_version}"}

dbs=''
services=''
while IFS= read -r gem; do
  if supported_database "${gem}"
  then
    if [ -n "${dbs}" ]
    then
      dbs="${dbs},\"$(database_for "${gem}")\""
    else
      dbs="\"$(database_for "${gem}")\""
    fi
  fi

  if supported_service "${gem}"
  then
    if [ -n "${services}" ]
    then
      services="${services},\"$(service_for "${gem}")\""
    else
      services="\"$(service_for "${gem}")\""
    fi
  fi
done <<< $(grep "^gem\s*" Gemfile | cut -d"'" -f2 | cut -d'"' -f2)

dbs="[${dbs}]"
services="[${services}]"

jq \
  --arg name ${project_name} \
  --arg type ${project_type} \
  --arg version ${project_type_version} \
  --arg upstream_base_image ${project_upstream_base_image} \
  --argjson dbs "${dbs}" \
  --argjson services "${services}" \
  -n \
  '{name:$name,type:$type,version:$version,upstream_base_image:$upstream_base_image,services:$services,dbs:$dbs}' > .devtools
