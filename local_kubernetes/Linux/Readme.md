local k3s development
###

[Installation](https://rancher.com/docs/k3s/latest/en/advanced/#using-docker-as-the-container-runtime)

The following installs k3s as a service, but does not automatically start it or enable it to restart on reboot.
Remove the INSTALL_* variables before installing if you want k3s to run all the time on your machine.

```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_SKIP_ENABLE=true INSTALL_K3S_SKIP_START=true sh -s - --docker
sudo systemctl start k3s
```

### setup KUBECONFIG

k3s creates a config that you can copy to your ./kube/config file, and chown it to your user:group. Once you set your KUBECONFIG environment variable in ${HOME}/.bashrc to ${HOME}/.kube/config, all future shells will use this as the global
kube configuration. All logins to other servers will be added to this file over time (although if a temporary login token
is used it may expire). **Note** if you already have ~/.kube/config (you have used kubectl before), you may want to research
how to merge these together, or just remove it and start over.

```bash
sudo cp /etc/rancher/k3s/k3s.yaml ${HOME}/.kube/config
sudo chown $(id -u):$(id -g) ${HOME}/.kube/config
chmod 700 ${HOME}/.kube/config
```

### Bootstrapping

You can bootstrap everything required for k3s development using the install.sh script located in this directory,
including all of the above.

### Issues

DUHS Cisco Anyconnect clobbers local network access (always has)
You can install openconnect on linux using the package managers for your distribution. You may also wish to install the
extensions for the windowing system that you are using (such as Gnome), and manage your VPN connection using the standard UI:
- openvpn
- network-manager-openvpn-gnome
- openconnect
- network-manager-openconnect-gnome

#### DHE VPN

You must ensure that you stop k3s before connecting to the DHE VPN, and then start it again after connecting. Otherwise
your containers will not be able to communicate with host urls behind the DHE firewall.

```
sudo service k3s stop
/usr/local/bin/k3s-killall.sh
# start vpn
sudo service k3s start
```

### Uninstall K3s
The following will completely uninstall k3s.
Check [Documentation](https://rancher.com/docs/k3s/latest/en/installation/uninstall/) for latest command
```
/usr/local/bin/k3s-uninstall.sh
```
