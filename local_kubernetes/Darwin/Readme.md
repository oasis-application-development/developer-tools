Working with Rancher Desktop

## Installation

[Installation](https://docs.rancherdesktop.io/installation/)

Nice, slick UI
Default is to use containerd, switch to dockerd for use with docker + docker-compose.
Automatically configures new terminals to symlink /var/run/docker.sock to the rancher 
socket.

### Issues

DUHS Cisco Anyconnect clobbers local network access (always has)
You can install openconnect on a mac using brew (I had to wrangle with directory permissions a lot, but finally got it to install)
```
brew install openconnect
```

Then I could connect to the vpn
```
sudo openconnect --user=$(ud -un) vpn.duhs.duke.edu
password: $netidpass
password: push (or click yubikey, maybe even fingerprint)
...
```
You have to leave this running, either in the foreground, or as a background process in your terminal.
