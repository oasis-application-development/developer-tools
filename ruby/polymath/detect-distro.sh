#!/bin/bash

if command -v apt-get > /dev/null 2>&1
then
  echo "deb"
elif command -v dnf > /dev/null 2>&1
then
  echo "rh"
else
  echo "UNKNOWN" >&2
  exit 1
fi
