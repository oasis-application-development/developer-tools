#!/bin/bash

detect_ruby_gemfile_version()
{
  input="$(grep '^ruby' Gemfile)"
  if [ $? -gt 0 ]
  then
    echo "You must include ruby '~> X.Y.Z' in your Gemfile for this to detect your ruby version" >&2
    return 1
  fi

  regex="^ruby.*~>.*([1-9]+.[0-9]+.[0-9]+).*$"
  if [[ $input =~ $regex ]]
  then
    ruby_version=${BASH_REMATCH[1]}
    echo "${ruby_version}"
  else
    echo "You must include ruby '~> X.Y.Z' in your Gemfile for this to detect your ruby version" >&2
    return 1
  fi
}

detect_ruby_dotfile_version()
{
  input=$(cat .ruby-version)
  regex="^ruby-([1-9]+.[0-9]+.[0-9]+)$"
  if [[ $input =~ $regex ]]
  then
    ruby_version=${BASH_REMATCH[1]}
    echo "${ruby_version}"
  else
    detect_ruby_gemfile_version
  fi
}

# try .ruby-version first, since its easier to parse
# fall-back to Gemfile
detect_ruby_version() {
  if [ -f .ruby-version ]
  then
    detect_ruby_dotfile_version
  else
    detect_ruby_gemfile_version
  fi
}

RUBY_VERSION=$(detect_ruby_version)
if [ $? -gt 0 ]
then
  exit 1
fi

RUBY_MAJOR_MINOR_VERSION=$(echo ${RUBY_VERSION%\.*})
ruby --version | grep ${RUBY_MAJOR_MINOR_VERSION} > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "installing RUBY_VERSION: ${RUBY_VERSION} RUBY_MAJOR_MINOR_VERSION: ${RUBY_MAJOR_MINOR_VERSION}"
  cd /tmp
  curl -s https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR_MINOR_VERSION}/ruby-${RUBY_VERSION}.tar.gz > /tmp/ruby-${RUBY_VERSION}.tar.gz
  tar -zxvf /tmp/ruby-${RUBY_VERSION}.tar.gz
  cd ruby-${RUBY_VERSION}
  ./configure && make && make install && make clean
  cd /tmp && rm -rf ruby-${RUBY_VERSION} ruby-${RUBY_VERSION}.tar.gz
  gem update --system
  echo "ruby ${RUBY_VERSION} installed"
else
  echo "ruby ${RUBY_VERSION} detected"
fi

cd ${APP_PATH}
echo "checking bundle version"
if [ -e Gemfile.lock ]
then
  BUNDLER_VERSION=$(grep -a1 'BUNDLED WITH' Gemfile.lock | tail -1 | cut -d' ' -f4)
  bundle version | grep ${BUNDLER_VERSION} > /dev/null 2>&1
  if [ $? -gt 0 ]
  then
    echo "installing bundle ${BUNDLER_VERSION}"
    gem install bundler:${BUNDLER_VERSION}
  else
    echo "bundle ${BUNDLER_VERSION} detected"
  fi
else
  echo "Gemfile.lock not found, using default $(bundle version)"
fi
