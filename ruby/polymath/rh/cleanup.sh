#!/bin/bash

dnf clean all
rm -rf `gem env gemdir`/cache/*.gem
if [ -d "$(gem env gemdir)/gems/" ]
then
  find `gem env gemdir`/gems/ -name "*.c" -delete
  find `gem env gemdir`/gems/ -name "*.o" -delete
fi