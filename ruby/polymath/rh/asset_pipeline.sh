#!/bin/bash

if [ -f "package.json" ]
then
  nodejs_major_version=16
  if [ -f ".nodejs-major-version" ]
  then
    nodejs_major_version=$(cat .nodejs-major-version)
  fi

  if ! (node --version | grep "v${nodejs_major_version}") > /dev/null 2>&1
  then
    echo "installing nodejs ${nodejs_major_version}" >&2
    dnf module install -y nodejs:${nodejs_major_version}/default
    npm install -g npm@next
  fi
fi

if [ -f "yarn.lock" ]
then
  if ! yarn --version > /dev/null 2>&1
  then
    echo "installing yarn" >&2
    dnf install -y nodejs-yarn
  fi
fi
