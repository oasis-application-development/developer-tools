#!/bin/bash

if [ -n "${BASH_SOURCE[0]}" ]
then
  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
else
  SCRIPT_DIR="${${(%):-%x}:A:h}"
fi
source ${SCRIPT_DIR}/architecture
first_arg="${1}"

docker-compose stop server
docker-compose rm -f server

case "${first_arg}" in
  --rebuild | -r)
    docker rmi "${PWD##*/}_server"
    ;;
  --help | -h)
    echo "usage: ${0} [--help] [-h] [--rebuild] [-r]"
    echo "  --help prints help and exits"
    echo "  --rebuild removes existing container image before build"
    exit
    ;;
esac

docker-compose build server
